
const Joi = require('joi');

const schema = Joi.object().keys({
    login:   Joi.string(),
    password:    Joi.string().min(8),
    email:   Joi.string().email(),
    firstname: Joi.string(),
    lastname:  Joi.string(),
    nir: Joi.number().integer()
})

module.exports = schema;