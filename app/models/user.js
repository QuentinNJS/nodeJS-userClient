'use strict';

const jsonToMongoose    = require('json-mongoose');
const async             = require('async');
const bcrypt            = require('bcrypt');
const mongoose          = require('k7-mongoose').mongoose();

module.exports = jsonToMongoose({
    mongoose    : mongoose,
    collection  : 'user',
    schema      : require('../schemas/user'),

    //Erreur dans l'autoinc, on garde les ID de mongodb
    /*autoinc     : {
        field : '_id', startAt: '1', incrementBy: '1'
    },*/
    pre         : {
        save : (doc, next) => {
            async.parallel({
                password : done => {
                    bcrypt.hash(doc.password, 10, (err, hash) => {
                        if (err) {
                            return next(err);
                        }
                        doc.password = hash;
                        done();
                    });
                }
            }, next);
        }
    },
    schemaUpdate : (schema) => {
        schema.login.unique  = true;
        schema.email.unique  = true;
        return schema;
    },
    transform : (doc, ret, options) => {
        delete ret.password;

        return ret;
    },
    options : {

    }
});