'use strict';

const handlerUser = require('../handlers/user');
const schema = require('../schemas/user.js');
const schemaUpdate = require('../schemas/userUpdate');
const Joi = require('joi');

exports.register = (server, options, next) => {
    server.route([
        {
            method  : 'GET',
            path    : '/users/',
            config  : {
                description : 'Index Users',
                notes       : 'Route affichant l\'index utilisateur',
                tags        : ['api'],
                handler     : handlerUser.getAllUsers
            }
        },
        {
            method : 'POST',
            path   : '/users/generate/{quantite}',
            config : {
                description : 'Genereration utilisateur' ,
                notes       : 'Generer un nombre choisi d\'utilisateurs',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                handler     : handlerUser.generate,
                validate: {
                    params: {
                        quantite: Joi.number().integer().max(100)
                    }
                },
            }
        },
        {
            method : 'POST',
            path   : '/users/',
            config : {
                description : 'Créer un utilisateur',
                notes       : 'Route de création d\'un user',
                tags        : [ 'api' ],
                handler: handlerUser.userCreate,
                validate: {
                    payload: schema
                },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                }
            }
        },
        {
            method  : 'GET',
            path    : '/users/{_id}',
            config  : {
                description : 'Trouver un user',
                notes       : 'Route de création d\'un user',
                tags        : ['api'],
                handler     : handlerUser.getOneUser,
                validate: {
                    params: {
                        _id: Joi.string().required()
                    }
                },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                }
            }
        },
        {
            method : 'PUT',
            path   : '/users/{_id}',
            config : {
                description : 'Update un utilisateur',
                notes       : 'Route de mise à jour d\'un user',
                tags        : [ 'api' ],
                handler: handlerUser.updateUser,
                validate: {
                    params:{_id: Joi.string().required()},
                    payload: schemaUpdate
                },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                }
            }
        },
        {
            method  : 'DELETE',
            path    : '/users/{_id}',
            config  : {
                description : 'Delete un user',
                notes       : 'Route de suppression d\'un user',
                tags        : ['api'],
                handler     : handlerUser.deleteUser,
                validate: {
                    params: {
                        _id: Joi.string().required()
                    }
                },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                }
            }
        },
        {
            method  : 'POST',
            path    : '/recover_password',
            config  : {
                description : 'Change et update le password',
                notes       : 'Route de récupération et d\'envoi de mail du mot de passe du user',
                tags        : ['api'],
                handler     : handlerUser.updatePassword,
                validate: {
                    payload: Joi.object().keys({
                        email: Joi.string().email().required(),
                        newPassword: Joi.string().required()
                    })
                },
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'forms'
                    }
                }
            }
        },
        {
            method  : 'POST',
            path    : '/authent',
            config  : {
                description : "Authentification",
                notes       : "Route permettant de se connecter",
                tags        : ['api'],
                handler     :  handlerUser.loginUser,
                validate: {
                    payload: Joi.object().keys({
                        login       :   Joi.string(),
                        password    :    Joi.string()
                    })
                },
                plugins: {
                    'hapi-swagger':{
                        payloadType: 'forms'
                    }
                }
            }
        }
    ]);
    next();
};

exports.register.attributes = {
    name: 'user-routes'
};