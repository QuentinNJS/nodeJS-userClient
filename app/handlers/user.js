'use strict';
const Bcrypt = require("bcrypt");
const Faker = require("faker");
const mongoose = require("k7-mongoose").mongoose();
const Boom = require('boom');
const _ = require("lodash");



module.exports.getAllUsers = (request,response) => {
    const User =  request.server.database.user;
    User.find({}, function(err, users) {
        if (err) {
            response(Boom.badGateway("Page Not Found"));
            throw err;
        }

        response(users).code(200);
    });
};

module.exports.userCreate = (request, reply) => {
    let u = new request.server.database.user();
    u.set(request.payload);
    let uncryptedPassword = request.payload.password;
    u.save().then(
        (saved) => {
            request.server.ioClient.emit('new-user', {user:saved, uncryptedPassword:uncryptedPassword});
            saved.uncryptedPassword = request.payload.password;
            reply({response:"created"}).code(201);

        }
    ).catch(
        (err) => {
            reply(Boom.badImplementation(err));
        });

};

module.exports.getOneUser = (request,response) => {
    const User = request.server.database.user;
    User.findOne({_id: request.params._id}, function(err,user){
        if(err) {
            response(Boom.badGateway("User Not Found"));
            throw err;
        }
        response({user:user}).code(200);

    });
};


module.exports.updateUser = (request,response)=> {
    const User = request.server.database.user;
    if(request.payload.password != undefined) request.payload.password = Bcrypt.hashSync(request.payload.password, 10);
    User.findOneAndUpdate({_id: request.params._id},request.payload)
        .then(
            (data) => {
                if (request.payload.password != undefined || request.payload.login != undefined) {
                    request.server.ioClient.emit('update-user', {user:data});
                }
                response({response:"utilisateur mis à jour"}).code(200);

            })
        .catch(
            (err) => {
                response(Boom.badData(err));
            }
        );
};

module.exports.deleteUser = (request,response) => {
    const User = request.server.database.user;
    User.findOneAndRemove({_id: request.params._id},function(err,user){
        if (err) {
            response(Boom.badImplementation("Delete error"));
            throw err;
        }
        response({response:"Utilisateur supprimé"}).code(204);
    })
};

module.exports.loginUser = (request,response) => {
    const User = request.server.database.user;
    User.findOne({login: request.payload.login},function (err,user){
        if(err) {
            response(Boom.badImplementation("erreur"));
            throw err;
        }

        Bcrypt.compare(request.payload.password, user.password,function(err,isMatch){
            if(err) throw err;
            isMatch ? response({response:"ok"}).code(200) : response(Boom.unauthorized("ko"));

        })
    })
};

module.exports.updatePassword = (request, response) => {
    const User = request.server.database.user;
    let newpw = Bcrypt.hashSync(request.payload.newPassword, 10);
    User.findOneAndUpdate({email: request.payload.email}, {password: newpw})
        .then(
            (data) =>
            {
                request.server.ioClient.emit('recover-user', {user:data});
                response({"info":"Mot de passe modifié, email envoyé"}).code(200);
            })
};

module.exports.generate = (request, reply) => {
    let users = [];
    for (let i = 0; i < parseInt(request.params.quantite); i++) {
        let user = new request.server.database.user();
        user.set({
            login: Faker.internet.userName(),
            password: Faker.internet.password(),
            email: Faker.internet.email(),
            firstname: Faker.name.firstName(),
            lastname: Faker.name.lastName()
        });
        users.push(user.save());
    }

    Promise.all(users).then(users => {
        reply(null, _.map(users, (user) => {
            return user.toObject();
        }));
    }).catch(err => {
        reply.badImplementation(err);
    });
};